<?php

namespace ChemicalsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Element from periodic table.
 *
 * @ORM\Table(name="chemicals_element")
 * @ORM\Entity(repositoryClass="ChemicalsBundle\Repository\ElementRepository")
 */
class Element
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255, unique=true)
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="formula", type="string", length=255, unique=true)
     */
    private $formula;

    /**
     * That method is not created automatically.
     *
     * Used as a way to keep consistency across the application.
     *
     * @return string
     */
    public function __toString()
    {
        return $this->getName() . '(' . $this->getFormula() . ')';
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Element
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set formula
     *
     * @param string $formula
     *
     * @return Element
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Get formula
     *
     * @return string
     */
    public function getFormula()
    {
        return $this->formula;
    }
}
