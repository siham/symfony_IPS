<?php

namespace ChemicalsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Atom
 *
 * @ORM\Table(name="atom")
 * @ORM\Entity(repositoryClass="ChemicalsBundle\Repository\AtomRepository")
 */
class Atom
{
    /**
     * @ORM\ManyToOne(targetEntity="ChemicalsBundle\Entity\Element")
     * @ORM\JoinColumn(nullable=false)
     */
    private $element;

    public function getElement()
    {
        return $this->element;
    }
    public function setElement($element)
    {
        $this->element = $element;
    }

    /*/**
     * @ORM\ManyToMany(targetEntity="ChemicalsBundle\Entity\Molecule", cascade={"persist"})
     */
    private $molecules;

   /* public function __construct($molecules)
    {
        $this->molecules = new ArrayCollection();

    }*/

    public function addMolecule(Molecule $molecule)
    {
        // Ici, on utilise l'ArrayMolecules comme un tableau
        $this->molecules[] = $molecule;

        return $this;
    }

    public function removeMolecule(Molecule $molecule)
    {
        // Ici on utilise une méthode de l'ArrayCollection, pour supprimer la molecule en argument

        $this->molecules->removeElement($molecule);
    }

    public function getMolecules()
    {
        return $this->molecules;
    }

    public function setMolecules($molecules)
    {
        $this->molecules = $molecules;
    }


    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="formula", type="string", length=255)
     */
    private $formula;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     */
    private $name;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set formula
     *
     * @param string $formula
     *
     * @return Atom
     */
    public function setFormula($formula)
    {
        $this->formula = $formula;

        return $this;
    }

    /**
     * Get formula
     *
     * @return string
     */
    public function getFormula()
    {
        return $this->formula;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Atom
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

}

