<?php

namespace ChemicalsBundle\Repository;

/**
 * ElementRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ElementRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * That class comes with built-in methods that can be used to
     * get the chemical elements from database :
     *     findById($id)
     *     findAll()
     *     findByName($name) - because Element has the "Name" attribute
     *     findByFormula($formula) - because Element has the "Formula" attribute
     */
}
