<?php
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use ChemicalsBundle\Entity\Molecule;

class LoadMolecule implements FixtureInterface
{
    // Dans l'argument de la méthode load, l'objet $manager est l'EntityManager
    public function load(ObjectManager $manager)
    {
        $tab=array(
            array('name'=>'Eau', 'formula'=>'H2O'),
            array('name'=>'Methane', 'formula'=>'CH4'),
            array('name'=>'Dichlore', 'formula'=>'Cl2'),
            array('name'=>'Diazote', 'formula'=>'N2')
        );

        // On crée la molecule

        foreach ($tab as $ligne) {
            $molecule = new Molecule();
            $molecule->setName($ligne['name']);
            $molecule->setFormula($ligne['formula']);

            $manager->persist($molecule);
        }

        // On déclenche l'enregistrement de toutes les molecules
        $manager->flush();
    }
}