<?php
namespace ChemicalsBundle\Controller;

use ChemicalsBundle\Form\ElementType;
use Doctrine\ORM\Mapping\Id;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use ChemicalsBundle\Entity\Element;
use Symfony\Component\HttpFoundation\Request;

/**
 * Element controller.
 *
 * @author Aurélien MULLER
 */
class ElementController extends Controller
{
    /**
     * Display elements from periodic table.
     *
     * @param type $page
     * @param type $maxPerPage
     *
     * @return type
     */
    public function listAction(Request $request)
    {
        // Let's get the element repository.
        $r = $this->getDoctrine()->getRepository("ChemicalsBundle:Element");
        $elements = $r->findAll();

        /**
         * @var $paginator |Knp|Component|Pager|Paginator
         */
        $elements = $this->get('knp_paginator')->paginate(
            $elements,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 10)

        );

        // Root path of render is app/Resources/views.
        return $this->render(
            'chemicals/elements.list.html.twig',
            [
                'elements' => $elements,
            ]);
    }

    /**
     * Asks a webservice for data about elements from Mendeleiev table.
     *
     * This data is then used to create entities: elements.
     */
    public function createSampleElementsFromWebserviceAction()
    {
        try {
            // Let's get the RESTHelper service.
            $ws = $this->get("chemicals.helper");
            $periodicElements = $ws->getPeriodicElements();

            if (!empty($periodicElements)) {
                // Doctrine Manager is used to 
                $em = $this->getDoctrine()->getManager();
                foreach ($periodicElements as $formula => $name) {
                    // Declaration of a new Element entity.
                    $element = new Element();
                    $element->setFormula($formula);
                    $element->setName($name);

                    // Index the entity within Doctrine.
                    // That method DOES NOT launch any request
                    // to MYSQL server.
                    $em->persist($element);
                }
                // Flush method sends all the stored requests to MySQL.
                $em->flush();
                // AddFlash is a method storing messages that are displayed once.
                // Check documentation to know how to display them within Twig template
                // (base.html.twig)
                $this->addFlash(
                    'notice',
                    'Atoms and periodic elements were created successfully.'
                );
            } else {
                $this->addFlash(
                    'warning',
                    'Nothing was acquired from webservice.'
                );
            }
        } catch (Exception $ex) {
            $this->addFlash(
                'error',
                'An error occurred while creating elements. Check the logs for more information.'
            );
            // Log that error.
            // $this->get('logger')->error($ex->getMessage());
        }

        return $this->redirectToRoute("chemicals_elements_list");
    }

    public function seeAction($id)
    {
        $rep = $this->getDoctrine()->getRepository("ChemicalsBundle:Element");
        //Possibilite #1.

        $element = $rep->findById($id);


        return $this->render('chemicals/elements.detail.html.twig',

            [
                'element' => $element,
            ]

        );

    }

//Methode ajout
    public function addAction(Request $request)
    {

        $element = new Element();
        $form = $this->createForm(ElementType::class, $element);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($element);
            $em->flush();
            unset($element);
            unset($form);
            $element = new Element();
            $form = $this->createForm(ElementType::class, $element);

//            return $this->redirectToRoute("chemicals_elements_list");
        }

        return $this->render('chemicals/AddElement.html.twig', [
                'form' => $form->createView()
            ]
        );

    }

//Methode suprimer
    public function deleteAction($id)
    {
        $rep = $this->getDoctrine()->getRepository("ChemicalsBundle:Element");
        $element = $rep->findById($id);

        $rep = $this->getDoctrine()->getRepository("ChemicalsBundle:Atom");
        $atoms = $rep->findAll();

        if (!empty($element)) {
            $em = $this->getDoctrine()->getManager();
            foreach ($element as $el) {
                $em->remove($el);
                $em->flush();

            }

        }

        // Root path of render is app/Resources/views.
        return $this->redirectToRoute("chemicals_elements_list");


    }

    //Methode modifier
    public function editAction($id, Request $request)
    {
        $element = new Element();

        $element = $this->getDoctrine()
            ->getRepository('ChemicalsBundle:Element')
            ->find($id);
        if (!$element) {
            throw $this->createNotFoundException(
                'cet element existe pas ' . $id
            );
        }

        $form = $this->createForm(ElementType::class, $element);
        $form->handleRequest($request);


        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            //$em->persist($element);
            $em->flush();

            return $this->redirectToRoute("chemicals_elements_list");

        }
        return $this->render('chemicals/edit.html.twig', [
                'form' => $form->createView()
            ]
        );

    }
}