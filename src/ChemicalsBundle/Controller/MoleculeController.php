<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 30/11/2016
 * Time: 21:47
 */

namespace ChemicalsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ChemicalsBundle\Form\MoleculeType;
use ChemicalsBundle\Entity\Molecule;
use Symfony\Component\HttpFoundation\Request;


class MoleculeController extends Controller
{
    public function listMAction(Request $request)
    {
        // Let's get the molecule repository.
        $r = $this->getDoctrine()->getRepository("ChemicalsBundle:Molecule");
        $molecules = $r->findAll();
        /**
         * @var $paginator|Knp|Component|Pager|Paginator
         */
        $molecules = $this->get('knp_paginator')->paginate(
            $molecules,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 5)

        );

        // Root path of render is app/Resources/views.
        return $this->render(
            'chemicals/molecules.listM.html.twig',

            [
                'molecules' => $molecules,
            ]);
    }

    public function seeAction($id)
    {
        $rep = $this->getDoctrine()->getRepository("ChemicalsBundle:Molecule");
        //Possibilite #1.

        $molecule = $rep->findById($id);

        return $this->render('chemicals/molecules.detail.html.twig',

            [
                'molecule' => $molecule,
            ]

        );
    }


//Methode ajout
    public function addAction(Request $request)
    {
        $molecule = new Molecule();

        $form = $this->createForm(MoleculeType::class, $molecule);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($molecule);
            $em->flush();
            unset($molecule);
            unset($form);
            $molecule = new Molecule();
            $form=$this->createForm(MoleculeType::class, $molecule);

            //return $this->redirectToRoute("chemicals_atoms_listA");
        }

        return $this->render('chemicals/AddMolecule.html.twig', [
                'form' => $form->createView()
            ]
        );

    }

    //Methode suprimer
    public function deleteAction($id)
    {
        $rep = $this->getDoctrine()->getRepository("ChemicalsBundle:Molecule");
        $molecule = $rep->findById($id);

        if (!empty($molecule)) {
            $em = $this->getDoctrine()->getManager();
            foreach ($molecule as $mol) {
                $em->remove($mol);
                $em->flush();

            }

        }
        return $this->redirectToRoute("chemicals_molecules_listM");
    }

    //Methode modifier
    public function editAction($id, Request $request)
    {
        $molecule = new Molecule();

        $molecule = $this->getDoctrine()
            ->getRepository('ChemicalsBundle:Molecule')
            ->find($id);
        if (!$molecule) {
            throw $this->createNotFoundException(
                'cette Molecule existe pas ' . $id
            );
        }

        $form = $this->createForm(MoleculeType::class, $molecule);
        $form->handleRequest($request);


        if ($form->isValid() && $form->isSubmitted()) {
            $em = $this->getDoctrine()->getManager();
            //$em->persist($molecule);
            $em->flush();

            return $this->redirectToRoute("chemicals_molecules_listM");

        }
        return $this->render('chemicals/edit.html.twig', [
                'form' => $form->createView()
            ]
        );


    }
}


