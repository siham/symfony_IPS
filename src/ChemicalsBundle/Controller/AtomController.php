<?php

namespace ChemicalsBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use ChemicalsBundle\Form\AtomType;
use ChemicalsBundle\Entity\Atom;
use Symfony\Component\HttpFoundation\Request;

class AtomController extends Controller
{
    public function listAAction(Request $request)
    {
        // Let's get the element repository.
        $r = $this->getDoctrine()->getRepository("ChemicalsBundle:Atom");
        $atoms = $r->findAll();
        //pagination
        /**
         * @var $paginator|Knp|Component|Pager|Paginator
         */
        $atoms = $this->get('knp_paginator')->paginate(
            $atoms,
            $request->query->getInt('page', 1),
            $request->query->getInt('limit', 2)

        );

        // Root path of render is app/Resources/views.
        return $this->render(
            'chemicals/atoms.listA.html.twig',
            [
                'atoms' => $atoms,
            ]);
    }

    public function seeAction($id)
    {
        $rep = $this->getDoctrine()->getRepository("ChemicalsBundle:Atom");
        //Possibilite #1.

        $atom = $rep->findById($id);

        return $this->render('chemicals/atoms.detail.html.twig',

            [
                'atom' => $atom,
            ]

        );
    }
    public function addAction(Request $request)
    {

        $atom = new Atom();
        $form = $this->createForm(AtomType::class, $atom);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($atom);
            $em->flush();
            unset($atom);
            unset($form);
            $atom = new Atom();
            $form=$this->createForm(AtomType::class, $atom);

           //return $this->redirectToRoute("chemicals_atoms_listA");
        }

        return $this->render('chemicals/AddAtom.html.twig', [
                'form' => $form->createView()
            ]
        );

    }


   //Methode suprimer
    public function deleteAction($id)
    {
        $rep = $this->getDoctrine()->getRepository("ChemicalsBundle:Atom");
        $atom = $rep->findById($id);

        if (!empty($atom)) {
            $em = $this->getDoctrine()->getManager();
            foreach ($atom as $at) {
                $em->remove($at);
                $em->flush();

            }

        }
        return $this->redirectToRoute("chemicals_atoms_listA");
    }

    //Methode modifier
    public function editAction($id, Request $request)
    {
        $atom = new Atom();

        $atom = $this->getDoctrine()
            -> getRepository('ChemicalsBundle:Atom')
            ->find($id);
        if (!$atom)
        {
            throw $this-> createNotFoundException(
                'cet element existe pas ' . $id
            );
        }

        $form = $this->createForm(AtomType::class, $atom);
        $form->handleRequest($request);


        if ($form->isValid()&&$form->isSubmitted())
        {
            $em=$this->getDoctrine()->getManager();
            //$em->persist($element);
            $em->flush();

            return $this->redirectToRoute("chemicals_atoms_listA");

        }
        return $this->render('chemicals/edit.html.twig', [
                'form'=>$form->createView()
            ]
        );

    }
}
